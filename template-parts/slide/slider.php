<div class="section-slider">
    <div id="sliderWinterControls" class="carousel slide" data-bs-ride="carousel">
        <div class="carousel-indicators">
            <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
        </div>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <a href="<?php echo home_url( '/productos' ); ?>">
                    <img src="<?php echo get_template_directory_uri() ?>/src/img/Banner-principal-wiinter.jpg" class="d-block w-100" alt="slider-winter-1">
                </a>
            </div>
            <!--<div class="carousel-item">
                <a href="<?php echo home_url( '/productos' ); ?>">
                    <img src="<?php echo get_template_directory_uri() ?>/src/img/banner-01.jpg" class="d-block w-100" alt="slider-winter-1">
                </a>
                <div class="carousel-caption d-none d-md-block">
                    <h5>Second slide label</h5>
                    <p>Some representative placeholder content for the second slide.</p>
                </div>
            </div> -->
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#sliderWinterControls" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#sliderWinterControls" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </button>
    </div> 
</div>