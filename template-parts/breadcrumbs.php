<?php if(is_single()): 
    $categories = get_the_category();
    $terms = get_term_by('id', $categories[0]->term_id, 'category');
    $bannerCat = get_field('banner_categoria', $terms);
    ?> 
    <div class="banner-cat">
        <div class="container-fluid p-0 m-0">   
            <div class="row">
                <img class="img-fluid w-100" src="<?php echo $bannerCat; ?>" alt="Banner Categoría <?php echo $categories[0]->name; ?>" />
            </div>
        </div> 
    </div> 
<?php endif; ?>


<?php if(is_category()): 
    $term = get_queried_object(); ?>
    
<div class="banner-cat">
    <div class="container-fluid p-0 m-0">   
        <div class="row">
            <?php
                //echo var_dump( $term);
                $bannerCat = get_field('banner_categoria', $term);
            ?>
            <img class="img-fluid w-100" src="<?php echo $bannerCat; ?>" alt="Banner Categoría <?php echo $term->name; ?>" />
        </div>
    </div> 
</div>
<div class="breadcrumbs pb-0 text-center">
    <div class="container"> 
        <div class="col-md-12 text-center">
            <div class="box-title">
                <h2 class="mb-0"><?php echo $term->name?></h2>     
                <hr>                  
                <a href=<?php echo home_url( '/productos' ); ?>" class="link-vertodos">Ver todos los productos</a>
            </div>
        </div> 
    </div>
</div>
<?php endif; ?>


<?php if(is_search()): 
    $term = get_queried_object(); ?> 
    
    <?php 
        get_template_part( 'template-parts/slide/slider', 'slider' ); 
    ?> 

    <div class="breadcrumbs py-4 pb-0 text-center">
        <div class="container"> 
            <div class="col-md-12 text-center">
                <div class="box-title">
                    <h2 class="mb-0">Buscando: <?php echo get_search_query()?></h2>     
                    <hr>                  
                    <a href=<?php echo home_url( '/productos' ); ?>" class="link-vertodos">Ver todos los productos</a>
                </div>
            </div> 
        </div>
    </div>
    <?php endif; ?>