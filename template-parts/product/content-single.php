<?php  if (is_single()):   ?> 
<div class="single">
    <div class="container">
        <div class="row">
                <div class="col-md-6">
                    <a href="<?php the_permalink(); ?>">
                        <img class="img-fluid img-producto" src="<?php echo get_field( "imagen" ) ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>" />
                    </a>
                </div>
                <div class="col-md-6">
                    <div class="info-detail py-4 px-4">
                        <h3 class="single-peso"><?php echo get_field( "peso" ) ?></h3>
                        <h1 class="single-title"><?php echo get_field( "nombre" ) ?></h1>
                        <div class="single-desc"><?php  echo get_field( "descripcion" )  ?></div>
                        <hr>
                        <?php   
                          /*  if(get_field("info_nutricional")):  ?>
                                <p>Informacion Nutricional</p>
                                <?php   
                                    foreach(get_field( "info_nutricional" ) as $key => $img):?>
                                        <img class="img-fluid" src="<?php echo $img['imagen'] ?>" alt="">
                                <?php   
                                    endforeach;  
                            endif;
                            */
                        ?> 
                    </div>
                </div>
        </div> 
    </div>
</div>
<div class="related-product py-5">
    <div class="container">
        <div class="box-title">
            <h2>Productos sugeridos</h2>                       
        </div> 
        <div class="row">
            <?php  
                $args=array( 
                        'post__not_in'      => array(get_the_ID()),
                        'post_type'         => 'producto',
                        'posts_per_page'    => 3,
                        'orderby'           => 'rand',
                    );
                $my_query = new WP_Query($args); 
                if( $my_query->have_posts() ):
                    while ( $my_query->have_posts() ) : $my_query->the_post();  
                        get_template_part( 'template-parts/product/content', 'related' );  
                    endwhile; 
                else:
                    get_template_part( 'template-parts/product/content', 'none' );  
                endif;
            ?>
        </div>                    
    </div>
</div> 
<?php  get_template_part( 'template-parts/related' ); 
        endif; ?>
 