<div class="col-md-4 col-xs-12 p-3">
    <div class="box related-product">
        <div class="row bloque-related">
            <div class="col-md-6"> 
                <img class="img-fluid" src="<?php echo get_field( "imagen" ) ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>" />             
            </div>
            <div class="col-md-6 my-auto">
                <div class="info-detail my-auto">
                    <h4 class="title mb-1"><?php the_title(); ?></h4> 
                    <p class="title mb-1"><?php echo get_field( "peso" ) ?></p> 
                    <a class="btn-more mb-2" href="<?php echo the_permalink() ?>"> Ver más</a>
                </div>
            </div>
        </div>
    </div>
</div> 