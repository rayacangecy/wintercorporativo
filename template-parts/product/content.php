<div class="col-md-4 col-xs-12">
    <div class="box cat-product p-5">
        <a href="<?php echo the_permalink() ?>">
            <img class="img-fluid w-100" src="<?php echo get_field( "imagen" ) ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>" />
        </a>
        <div class="info-detail my-auto">
            <h4 class="nombre"><?php the_title(); ?></h4> 
            <h6 class="peso">&nbsp;<?php echo get_field( "peso" ) ?></h6>            
            <a class="btn-more" href="<?php echo the_permalink() ?>"> Ver más</a>
        </div>
    </div>
</div> 