<?php
/* database */
$server = "localhost";
$user = "root";
$password = "root";
$dbase = "winter-corp";

try {
    $db = new PDO("mysql:host=${server};dbname=${dbase};charset=utf8", $user, $password);
} catch (PDOException $e) {
    echo $e->getMessage();
}
