<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Winter</title>
    <?php wp_head(); ?> 
</head>
<body id="bodyInfo" data-url='<?= get_bloginfo("template_url"); ?>'>  

<nav class="navbar navbar-expand-lg nav-menu fixed-top">
  <div class="container-fluid">
   


          <a class="open-menu" data-bs-toggle="offcanvas" href="#offcanvasExample" role="button" aria-controls="offcanvasExample"></a>

          <a class="nav-link logo-winter" href="<?php echo home_url( '/' ); ?>">
            <img class="logo-black" src="<?php echo get_template_directory_uri(); ?>/src/img/logo-winter.png" alt="Logo Winter">
          </a>

          <form class="d-flex search-winter" action="<?php echo home_url( '/' ); ?>">
              <input class="form-control form-search" type="search" placeholder="Buscar" aria-label="Search" name="s">
              <input type="hidden" name="post_type" value="producto" />
              <button class="btn btn-outline-success" type="submit"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-search" viewBox="0 0 16 16">
                  <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/>
                  </svg>
              </button>
          </form> 

  </div>
</nav>


<div class="offcanvas offcanvas-start" tabindex="-1" id="offcanvasExample" aria-labelledby="offcanvasExampleLabel">
  <div class="offcanvas-header">
    <h5 class="offcanvas-title" id="offcanvasExampleLabel"></h5>
    <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
  </div>
  <div class="offcanvas-body p-0">
    
  <?php
				$post_type = 'producto';
				$taxonomy = 'category';
				$terms = get_terms( array(
				    'taxonomy' => $taxonomy,
				    'hide_empty' => true,
				    'exclude'  => array(1)
				));
				if(! empty($terms) && !is_wp_error($terms)): ?>
				    <ul class="ctx-menu">
				        <?php foreach($terms as $term) {
					        $parent_term_id = $term->term_id;
					        $parent_term_slug = $term->slug; ?>
				            <li class="menu-item has-children">
				            		<a href="<?php echo get_term_link($term->slug, $taxonomy); ?>">
					            		<?php 
                            $icon_cat = get_field('imagen', $term); 
                            $icon_cat_hover = get_field('imagen_hover', $term); 
                          ?>
					            		<div class="clearfix d-flex align-items-center">
						            		<?php if (!empty($icon_cat)): ?>
							            		<div class="item-icon">
								            		<img class="img-fluid icowhite" src="<?php echo $icon_cat; ?>" alt="Icono categoría <?php echo $term->name; ?>" />
                                <img class="img-fluid icoblack" src="<?php echo $icon_cat_hover; ?>" alt="Icono categoría <?php echo $term->name; ?>" />
							            		</div>
						            		<?php endif; ?>
						            		<div class="clearfix item-text">
							            		<?php echo $term->name; ?>
						            		</div>
					            		</div>
					            	</a>
				            </li>
				        <?php } ?>
				    </ul>
				<?php endif; wp_reset_postdata(); ?>
  </div>
</div>