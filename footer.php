        <!-- FOOTER -->
        <footer>  
            <div class="bloque-footer">
                <div class="container py-5">
                    <div class="row d-flex align-items-center">                        
                        <div class="col-md-6 col-xs-12">
                            <img class="logo-cial" src="<?php echo get_template_directory_uri(); ?>/src/img/cial-logo-footer.png" alt="Logo Cial"/>
                            <a href="<?php echo home_url('/contacto'); ?>" class="link-foot">Contáctanos</a>
                        </div>
                        <div class="col-md-6 col-xs-12 text-end">
                            <h5 class="m-0">Dirección</h5>
                            <p>Américo Vespucio 2341, Pudahuel. Santiago</p>
                            <h5 class="m-0">Teléfono</h5>
                            <p>800072425</p>
                            <h5 class="m-0">Correo electrónico</h5>
                            <p>sac@cialalimentos.cl</p>
                            
                            <ul class="ctx-rrss d-flex justify-content-end">
                                <li><a href="javascript:void(0);" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/src/img/ico-yt.png"></a></li>
                                <li><a href="javascript:void(0);" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/src/img/ico-fb.png"></a></li>
                            </ul>
                            
                        </div>
                        <div class="col-md-12 col-xs-12 text-center">
                            <div class="copy">©2021 Todos los derechos reservados Winter.</div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>

        <div class="msg-healt"> 
            <div class="container py-2"> 
                <p class="text-center m-0"><img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/src/img/993px-MINSAL.png" alt=""> Prefiere alimentos con menos sellos de advertencia</p>
            </div>
        </div>

    </main> 
    <?php wp_footer() ?> 
</body>
</html>