<?php 

/* add master css and js*/
function add_theme_scripts() { 
    wp_enqueue_style( 'css', get_template_directory_uri() . '/dist/css/main.min.css', array(), '1.1', 'all');
    wp_enqueue_script( 'script', get_template_directory_uri() . '/dist/js/bundle.min.js', array('jquery'));
}

add_action( 'wp_enqueue_scripts', 'add_theme_scripts' ); 


// Register Custom Post Type
function productos_post_type() {

	$labels = array(
		'name'                  => _x( 'Productos', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Producto', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Productos', 'text_domain' ),
		'name_admin_bar'        => __( 'Productos', 'text_domain' ),
		'archives'              => __( 'Item Archives', 'text_domain' ),
		'attributes'            => __( 'Item Attributes', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
		'all_items'             => __( 'Todo los productos', 'text_domain' ),
		'add_new_item'          => __( 'Añadir Nuevo Producto', 'text_domain' ),
		'add_new'               => __( 'Añadir Nuevo', 'text_domain' ),
		'new_item'              => __( 'New Item', 'text_domain' ),
		'edit_item'             => __( 'Edit Item', 'text_domain' ),
		'update_item'           => __( 'Update Item', 'text_domain' ),
		'view_item'             => __( 'View Item', 'text_domain' ),
		'view_items'            => __( 'View Items', 'text_domain' ),
		'search_items'          => __( 'Search Item', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Producto', 'text_domain' ),
		'description'           => __( 'Productos de Winter', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor' ),
		'taxonomies'            => array( 'category', 'post_tag' ),
        'menu_icon'             => 'dashicons-products',
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'producto', $args );

}
add_action( 'init', 'productos_post_type', 0 );


function searchfilter($query) {
    if ($query->is_search) {
        if(isset($_GET['post_type'])) {
            $type = $_GET['post_type'];
                if($type == 'producto') {
                    $query->set('post_type',array('producto'));
                }
        }       
    }
return $query;
}
add_filter('pre_get_posts','searchfilter');