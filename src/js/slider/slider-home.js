import Swiper , { Navigation } from 'swiper';
import "swiper/swiper-bundle.css";

class slideHome extends HTMLElement {
  constructor() {
    super();
  }

  connectedCallback() {
    Swiper.use([Navigation]);
    const swiper = new Swiper(".swiper__categories", {
      observer: true, 
      observeParents: true,
      slideToClickedSlide: true,
      navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
      },
      // Responsive breakpoints
      breakpoints: {
        // when window width is >= 320px
        320: {
          slidesPerView: 1,
          spaceBetween: 20
        },
        // when window width is >= 480px
        480: {
          slidesPerView: 1,
          spaceBetween: 30
        },
        // when window width is >= 640px
        640: {
          slidesPerView: 2,
          spaceBetween: 40
        },
        1024: {
          slidesPerView: 3,
          spaceBetween: 40
        }
      }
    });
  }
}

window.customElements.define("slider-home", slideHome);