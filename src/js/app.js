import "../../node_modules/bootstrap/dist/js/bootstrap.bundle";
import '../scss/app.scss'
import './slider/slider-home'

const URL_BASE = "http://localhost:8888/Raya/clientes/inflamable/winter/corporativo/wp-content/themes/winter/";


class formRegister extends HTMLElement {
    constructor() {
      super();
    }
  
    connectedCallback() {
      const formRegistro = document.getElementById("formRegistro");
      formRegistro.addEventListener("submit", (e) => {
        e.preventDefault();
        saveUser();
      });
  
      const saveUser = () => {
        const BASE_URL = window.location.origin;
        const data = new FormData();
        data.append("task", "saveUser");
        data.append("emailForm", document.getElementById("emailForm").value);
        data.append("telForm", document.getElementById("telForm").value);
        data.append("asuntoForm", document.getElementById("asuntoForm").value);
        data.append("msgForm", document.getElementById("msgForm").value);
        fetch(URL_BASE+"ajax/functions.php", {
          method: "POST",
          body: data,
        })
          .then((res) => res.json())
          .then((data) => {
            //console.log(data);
            formRegistro.reset();
            alert("Datos enviados correctamente...");
            
          })
          .catch((err) => console.log(err));
      };
  
      const inputFono = document.getElementById("telForm");
      inputFono.addEventListener("focus", (event) => {
        inputFono.value = "+569";
      });
      
      
      inputFono.addEventListener('keypress', (e) => {
          if (!soloNumeros(e)) {
              e.preventDefault();
          }
      });
      
      function soloNumeros(e) {
          const key = e.charCode;
          //console.log(key);
          return key >= 48 && key <= 57;
      }
    }
  }
  window.customElements.define("form-register", formRegister);
  

