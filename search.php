<?php get_header(); ?> 
<?php  get_template_part( 'template-parts/breadcrumbs' );?>
<div class="category-product py-5"> 
    <div class="container">   
        <div class="row g-2"> 
        <?php  
            if( have_posts() ):
                while ( have_posts() ) : the_post();  
                    get_template_part( 'template-parts/product/content' );  
                endwhile; 
            else:
                get_template_part( 'template-parts/product/content', 'none' );  
            endif;
        ?>
        </div><!-- /.row --> 
    </div><!-- /.container -->  
</div><!-- /.category -->  
<?php get_footer(); ?>