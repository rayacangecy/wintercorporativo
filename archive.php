<?php get_header(); ?> 
<?php  get_template_part( 'template-parts/breadcrumbs' );?>
<div class="category-product py-5"> 
    <div class="container">   
        <div class="row g-2"> 
        <?php   
        $term = get_queried_object();
        $args = array(
            'post_type' => 'producto',
            'posts_per_page' => -1,  
            'tax_query' => array( 
                array(
                    'taxonomy' => $term->taxonomy,
                    'field' => 'term_id',   
                    'terms' => $term->term_id,
                )
            )
        );
        $the_query = new WP_Query($args); 
        if( $the_query->have_posts() ):
            while ( $the_query->have_posts() ) : $the_query->the_post();  
                get_template_part( 'template-parts/product/content' );  
            endwhile; 
        else:
            get_template_part( 'template-parts/product/content', 'none' );  
        endif;
        ?>
        </div><!-- /.row --> 
    </div><!-- /.container -->  
</div><!-- /.category -->  
<?php get_footer(); ?>