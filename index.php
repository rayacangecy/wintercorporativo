<?php get_header(); ?>

<?php 
    get_template_part( 'template-parts/slide/slider', 'slider' ); 
?> 

<div class="section-slider-categories">
    <div class="container-fluid bloque-slide">
        <!-- Slider main container -->
        <div class="swiper-container swiper__categories">
        <!-- Additional required wrapper -->
        <div class="swiper-wrapper">
            <!-- Slides -->
            <?php 
                $categories = get_categories( array(
                    'orderby' => 'name',
                    'order'   => 'ASC',
                    "hide_empty" => 0,
                    'exclude'  => array(1)
                ) );  

                
                foreach($categories as $k => $category): ?>
                    <div class="swiper-slide">
                        <div class="box-carrousel">
                            <img src="<?php echo the_field('imagen', $category);?>" alt="">
                            <h4><?php echo $category->cat_name ?></h4>
                            <p><?php echo $category->category_description ?></p>
                            <a href="<?php echo get_term_link($category); ?>" class="btn-vermas">Ver más</a>                       
                        </div>
                    </div>     
            <?php endforeach; ?>  
        </div>
        <!-- If we need pagination -->
        <div class="swiper-pagination"></div>
        <!-- If we need navigation buttons -->
        <div class="swiper-button-prev"></div>
        <div class="swiper-button-next"></div>
        </div> 
        <slider-home></slider-home> 
    </div>
</div> 


<?php get_footer(); ?>