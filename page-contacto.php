<?php /* Template Name: Page Contacto */ ?>
<?php get_header(); ?>

<?php 
    get_template_part( 'template-parts/slide/slider', 'slider' ); 
?> 

<div class="page-contacto py-5"> 
    <div class="container-fluid no-gutters">
        <div class="row">
            <div class="col-12 titulo">Solicitud de Contacto</div>
        </div>
        <form class="row g-3" id="formRegistro">
            <div class="mb-3">
                <label for="exampleFormControlInput1" class="form-label">Correo electronico</label>
                <input type="email" class="form-control" id="emailForm" placeholder="name@example.com" required>
            </div>
            <div class="mb-3">
                <label for="exampleFormControlInput1" class="form-label">Teléfono</label>
                <input type="text" class="form-control" id="telForm" placeholder="+569xxxxxxxx" maxlength="12" required>
            </div>
            <div class="mb-3">
                <label for="exampleFormControlInput1" class="form-label">Asunto</label>
                <input type="text" class="form-control" id="asuntoForm" placeholder="Ej: Consulta" reqrequireduire>
            </div>
            <div class="mb-3">
                <label for="exampleFormControlTextarea1" class="form-label">¿Cómo podemos ayudarte?</label>
                <textarea class="form-control" id="msgForm" rows="3" required></textarea>
            </div>
            <div class="mb-3">
                <button type="submit" class="btn btn-primary btn-enviar">Enviar</button>
            </div>
            <form-register></form-register>
        </form>
    </div><!-- /.container -->  
</div><!-- /.category -->  
<?php get_footer(); ?>