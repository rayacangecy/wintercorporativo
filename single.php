<?php get_header(); 
get_template_part( 'template-parts/breadcrumbs' );

if ( have_posts() ) { 
    while (have_posts()) {
        the_post();
        get_template_part( 'template-parts/product/content-single', get_post_type() );
    }
}
 
get_footer();
?> 